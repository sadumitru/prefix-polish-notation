#include <iostream>
#include "BTree.h"

int main() {
    BTree tree;
    tree.readFile("../input");
    tree.buildTree();

    std::cout << "Result: " << tree.calculate() << "\n";

    return 0;
}
