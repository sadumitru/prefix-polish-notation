#pragma clang diagnostic push
#pragma ide diagnostic ignored "modernize-use-nodiscard"
#ifndef PROJECTIII_BTREE_H
#define PROJECTIII_BTREE_H

#include <string>
#include <cmath>
#include <fstream>
#include <stack>
#include <sstream>
#include <ostream>
#include "Node.h"

class BTree {
private:
    Node *root;
    std::string expression;
    std::string digits = "0123456789";
    std::string operators = "^/*-+";

    std::string getNumber(unsigned long start_pos) const {
        std::size_t const end_pos = expression.find_first_not_of(digits, start_pos);

        return expression.substr(start_pos, end_pos != std::string::npos ? end_pos - start_pos : end_pos);
    }

    Node *add(Node **n, int *i) {
        while (expression[*i] == '(' || expression[*i] == ')' || expression[*i] == ' ') {
            (*i)++;
        }

        if (isDigitAtIndex(*i)) {
            return new Node(getNumber(*i));
        } else {
            if (*n == nullptr) {
                *n = new Node(expression[*i]);
            }

            (*i)++;
            Node *left = add(&(*n)->left, i);

            std::size_t const m = expression.find_first_not_of(digits, *i);
            *i = m + 1;
            Node *right = add(&(*n)->right, i);

            (*n)->left = left;
            (*n)->right = right;

            return *n;
        }
    }

    static bool validExpression(const std::string &expr) {
        std::stack<char> stack;

        for (auto &c : expr) {
            if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                stack.pop();
            }
        }

        return stack.empty();
    }

    bool validCharacters(const std::string &expr) {
        std::string validCharacters = digits + operators + "() ";

        for (auto &c : expr) {
            if (validCharacters.find(c) == std::string::npos) {
                return false;
            }
        }

        return true;
    }

    void inOrder(Node *p) {
        if (p == nullptr) {
            return;
        } else {
            std::cout << p->token << " ";
            inOrder(p->left);
            inOrder(p->right);
        }
    }

    bool isDigitAtIndex(int i) const { return digits.find(expression[i]) != std::string::npos; }

    double calculate(Node *p) {
        if (!p) {
            return 0;
        }

        if (!p->left && !p->right) {
            return toDouble(p->token);
        }

        double l_value = calculate(p->left);
        double r_value = calculate(p->right);

        if (p->token == "+") {
            return l_value + r_value;
        } else if (p->token == "-") {
            return l_value - r_value;
        } else if (p->token == "/") {
            return l_value / r_value;
        } else if (p->token == "*") {
            return l_value * r_value;
        }

        return pow(l_value, r_value);
    }

    double toDouble(const std::string &str) {
        std::string::size_type sz;
        return std::stod(str, &sz);
    }

public:

    BTree() {
        root = nullptr;
    }

    void readFile(const std::string &file) {
        std::string line;
        std::ifstream input_file(file);
        unsigned int lines = 0;

        if (input_file.is_open()) {
            while (getline(input_file, line) && ++lines <= 1) {
                expression = line;
            }
            input_file.close();

            if (lines != 1) {
                std::cout << "Invalid file format (1 line only).\n";
                exit(1);
            }
            if (!validCharacters(expression)) {
                std::cout << "Invalid characters in expression (digits, brackets and basic signs allowed).\n";
                exit(2);
            }
            if (!validExpression(expression)) {
                std::cout << "Expression was not built properly.\n";
                exit(3);
            }
        } else {
            std::cout << "Unable to open file \"" << file << "\".\n";
            exit(4);
        }
    }

    void buildTree() {
        int i = 0;
        add(&root, &i);
    }

    void printInOrder() {
        inOrder(root);
        std::cout<<'\n';
    }

    double calculate() {
        return calculate(root);
    }
};

#endif //PROJECTIII_BTREE_H

#pragma clang diagnostic pop