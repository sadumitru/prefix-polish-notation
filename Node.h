#ifndef PROJECTIII_NODE_H
#define PROJECTIII_NODE_H


#include <ostream>
#include <utility>

class Node {
public:
    std::string token;
    Node *left, *right;

    Node() {
        token = "";
        left = right = nullptr;
    }

    Node(std::string token) : token(std::move(token)) {
        left = right = nullptr;
    }

    Node(char token) {
        std::string tk;
        tk.push_back(token);
        this->token = tk;
        left = right = nullptr;
    }

    std::string getToken() const {
        return token;
    }

    void setToken(char token) {
        Node::token = token;
    }

    Node *getLeft() const {
        return left;
    }

    void setLeft(Node *left) {
        Node::left = left;
    }

    Node *getRight() const {
        return right;
    }

    void setRight(Node *right) {
        Node::right = right;
    }

    friend std::ostream &operator<<(std::ostream &os, const Node &node) {
        os << "token: '" << node.token << "', left: " << node.left << ", right: " << node.right;
        return os;
    }
};


#endif //PROJECTIII_NODE_H
